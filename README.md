# Elevation Profile
A simple way to compare elevation profiles.

## TODO
- Parse single GPX files
- Parse GPX files with multiple tracks
- Parse GPX files with elevation (Strava)
- Label total distance, elevation gained and lost
- Change the zeroing of the zeroed chart so that the lowest point of any profile is 0 (nothing should go below 0)
- Output to file directly, rather than use output redirection
- Documentation
- Cache activity ID streams for x number of days to save on API calls

## Ideas
- Reorganize
- Set up GitLab CI/Pipelines
- Wiki, or will Readme suffice?
- Google Docs, add Strava (route|segment|activity) ID, automatically parse and update a graph in the doc.
- Use Stravalib? http://pythonhosted.org/stravalib/
