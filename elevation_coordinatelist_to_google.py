import elevation_config
import coords_diedamskopf
import coords_hochaelpelekopf
import coords_hochberg
import coords_hohe_kugel
import coords_pfaender
import coords_zitterklapfen
import coords_saentis
import coords_schneiderkopf
import coords_winterstaude_attempt
import coords_brueggelekopf

import coords_ride_eichenberg
import coords_ride_pfaender

# import google_elevations_zitterklapfen

# import polyline
import requests
import urllib
import math

import pprint
import sys



BASE_URL = 'https://maps.googleapis.com/maps/api/elevation/json'
GOOGLE_LOCATIONS_LIMIT = 512

STRAVA_BASE_URL = 'https://www.strava.com/api/v3'


def main():
    # strava_stream = get_strava_stream('433827320', 'activities')

    #
    # output_strava_stream_to_google_dt_array(strava_stream)
    strava_stream_ids = {
        # '1154876561': 'activities', # Brüggelekopf
        # '692109037': 'activities', # Säntis
        '1154876561': 'activities',
        '1561756': 'segments',
        '692109037': 'activities',
        '1143019789': 'activities'
    }
    strava_streams = get_multiple_streams(strava_stream_ids)
    multiple_output_strava_stream_to_google_dt_array(strava_streams)
    sys.exit()

    # Lat/Lng list of tuples that have already been extracted from a GPX file
    profiles = [
        # coords_diedamskopf.coordinate_list,
        # coords_hochaelpelekopf.coordinate_list,
        # coords_hochberg.coordinate_list,
        # coords_hohe_kugel.coordinate_list,
        # coords_pfaender.coordinate_list,
        # coords_zitterklapfen.coordinate_list,
        # coords_saentis.coordinate_list,
        # coords_schneiderkopf.coordinate_list,
        # coords_winterstaude_attempt.coordinate_list,
        # coords_brueggelekopf.coordinate_list
        coords_ride_eichenberg.coordinate_list,
        coords_ride_pfaender.coordinate_list
    ]

    google_elevations = {}
    for profile in profiles:
        coordinate_list = profile['coords']
        name = profile['name']

        # Query Google for elevation data at the given coordinates
        google_elevations[name] = get_google_elevations(coordinate_list)
        # Fetch the elevation data from already saved results so we don't query Google
        # google_elevations = google_elevations_zitterklapfen.google_elevations

        # Add the distance to the dictionary
        distance = get_distance(google_elevations[name])

        # Write a JS datatable array string for a single profile
        # output_to_google_dt_array(google_elevations[name])

    # Write multiple profiles to a JS datatable
    multiple_output_to_google_dt_array(google_elevations)


def get_strava_activity_info(id):
    url = '{}/activities/{}'.format(STRAVA_BASE_URL, id)
    headers = {'Authorization': 'Bearer {}'.format(elevation_config.STRAVA_API_KEY)}

    r = requests.get(url, headers=headers)

    return r.json()


def multiple_output_strava_stream_to_google_dt_array(strava_streams):
    num_streams = len(strava_streams)
    name_mapping = []
    stream_distance = {}
    stream_altitude = {}
    output_string = "var google_array = [['Distance',"
    for stream_id, values in strava_streams.items():
        activity_info = _get_strava_activity_info(stream_id, values['type'])
        stream_name = activity_info['name']
        # stream_name = stream_name.decode('utf-8')
        # stream_name = urllib.parse.quote_from_bytes(stream_name)
        name_mapping.append(stream_name)
        for stream in values['stream']:
            if stream['type'] == 'distance':
                stream_distance[stream_name] = stream['data']
            elif stream['type'] == 'altitude':
                stream_altitude[stream_name] = stream['data']
        output_string += " '{}',".format(stream_name)
    output_string += "],"

    combined_profiles = {}
    for stream_name, values in stream_distance.items():
        for distance in values:
            if distance not in combined_profiles:
                combined_profiles[distance] = ['null'] * num_streams
            combined_profiles[distance][name_mapping.index(stream_name)] = stream_altitude[stream_name][stream_distance[stream_name].index(distance)]

    for distance_index in sorted(combined_profiles):
        output_string += "[{},".format(distance_index)
        for i in range(num_streams):
            output_string += "{},".format(combined_profiles[distance_index][i])
        output_string += "],"
    output_string += "]"

    print(output_string)


def _get_strava_activity_info(id, type):
    url = '{}/{}/{}'.format(STRAVA_BASE_URL, type, id)
    headers = {'Authorization': 'Bearer {}'.format(elevation_config.STRAVA_API_KEY)}

    r = requests.get(url, headers=headers)

    return r.json()


def output_strava_stream_to_google_dt_array(strava_stream):
    output_string = "var google_array = [['Distance', 'Elevation'],"
    for i in range(strava_stream[0]['original_size']):
        output_string += "[{},{}],".format(strava_stream[0]['data'][i], strava_stream[1]['data'][i])
    output_string += "]"

    print(output_string)


def multiple_output_to_google_dt_array(google_elevations):
    num_profiles = len(google_elevations)
    name_mapping = []
    output_string = "var google_array = [['Distance',"
    for name in google_elevations:
        name_mapping.append(name)
        output_string += " '{}',".format(name)
    output_string += "],"

    combined_profiles = {}
    for name, profile in google_elevations.items():
        for i, profile_data in enumerate(profile):
            if profile_data['distance'] not in combined_profiles:
                combined_profiles[profile_data['distance']] = ['null'] * num_profiles
            combined_profiles[profile_data['distance']][name_mapping.index(name)] = profile_data['elevation']

    for distance_index in sorted(combined_profiles):
        output_string += "[{},".format(distance_index)
        for i in range(num_profiles):
            output_string += "{},".format(combined_profiles[distance_index][i])
        output_string += "],"
    output_string += "]"
    print(output_string)


def output_to_google_dt_array(google_elevations):
    output_string = "var google_array = [['Distance', 'Elevation'],"
    for coords in google_elevations:
        output_string += "[{},{}],".format(coords['distance'], coords['elevation'])
    output_string += "]"

    print(output_string)


def get_strava_stream(id, type):
    STREAM_TYPES = ['altitude', 'distance']
    url = '{}/{}/{}/streams/{}'.format(STRAVA_BASE_URL, type, id, ','.join(STREAM_TYPES))
    headers = {'Authorization': 'Bearer {}'.format(elevation_config.STRAVA_API_KEY)}

    r = requests.get(url, headers=headers)

    return r.json()


def get_multiple_streams(strava_stream_ids):
    streams = {}

    for id, type in strava_stream_ids.items():
        streams[id] = {'type': type}
        streams[id]['stream'] = get_strava_stream(id, type)

    return streams


def get_google_elevations(coordinate_list):
    google_elevations = []

    for url in get_query_url_iter(coordinate_list):
        r = requests.get(url)
        google_elevations.extend(r.json()['results'])

    return google_elevations


def get_distance(google_elevations):
    total_distance_km = 0

    for i, coords in enumerate(google_elevations):
        (lat1, lon1) = (coords['location']['lat'], coords['location']['lng'])
        google_elevations[i]['distance'] = total_distance_km
        try:
            (lat2, lon2) = (google_elevations[i + 1]['location']['lat'], google_elevations[i + 1]['location']['lng'])
        except IndexError:
            # End of the coordinate list
            break

        # print('Segment distance: {}'.format(get_distance_between_coordinates(lat1, lon1, lat2, lon2)))
        total_distance_km += get_distance_between_coordinates(lat1, lon1, lat2, lon2)

    # print('Total distance: {}'.format(total_distance_km))

    return total_distance_km


def get_distance_between_coordinates(lat1, lon1, lat2, lon2):
    R = 6371
    dLat = degrees_to_radians(lat2 - lat1)
    dLon = degrees_to_radians(lon2 - lon1)
    a = math.sin(dLat / 2) * math.sin(dLat / 2) + \
        math.cos(degrees_to_radians(lat1)) * math.cos(degrees_to_radians(lat2)) * \
        math.sin(dLon / 2) * math.sin(dLon / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = R * c

    return d


def degrees_to_radians(deg):
    return deg * (math.pi / 180)


def get_query_url_iter(coordinate_list):
    index_start = 0
    index_end = GOOGLE_LOCATIONS_LIMIT

    while coordinate_list[index_start:index_end]:
        # print(index_start, index_end)
        path_string = get_coords_string_polyline(coordinate_list[index_start:index_end])
        url_path = '{}?key={}&path={}'.format(BASE_URL, elevation_config.GOOGLE_API_KEY, path_string)

        index_start = index_end
        index_end += GOOGLE_LOCATIONS_LIMIT

        yield url_path


def get_coords_string_polyline(coordinate_list):
    coords_string = polyline.encode(coordinate_list)

    return 'enc:{}&samples={}'.format(coords_string, len(coordinate_list))


if __name__ == '__main__':
    main()
